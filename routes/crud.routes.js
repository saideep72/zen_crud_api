

module.exports = (app) => {
    const cruds = require('../controllers/crud.controller.js');

    app.post('/', cruds.create);

    app.get('/', cruds.findAll);

    app.get('/:crudId', cruds.findOne);

    app.put('/:crudId', cruds.update);

    app.delete('/:crudId', cruds.delete);
}

const Crud = require('../models/crud.model.js');


exports.create = (req, res) => {
  if(!req.body.name || !req.body.type || !req.body.favorite) {
          return res.status(400).send({
              message: "name,type and favorite fields cannot be empty"
          });
      }
    const crud = new Crud({
        name: req.body.name,
        type: req.body.type,
        favorite: req.body.favorite
    });

    crud.save()
    .then(data => {
       res.send(data);
    }).catch(err => {
      res.status(500).send({
         message: err.message || "Some error occurred while creating the data."
     });
    });
};



exports.findAll = (req, res) => {

  Crud.find()
  .then(crud => {
    res.send(crud);
  }).catch(err => {
    res.status(500).send({
         message: err.message || "Some error occurred while retrieving data."
     });
  });

};


exports.findOne = (req, res) => {

    Crud.findById(req.params.crudId)
    .then(crud => {
      res.send(crud);
    }).catch(err => {
      res.status(500).send({
           message: err.message || "Some error occurred while retrieving data."
       });
    });
};

exports.update = (req, res) => {

  if(!req.body.name || !req.body.type || !req.body.favorite) {
          return res.status(400).send({
              message: "name,type and favorite fields cannot be empty"
          });
      }

    Crud.findByIdAndUpdate(req.params.crudId, {

      name: req.body.name,
      type: req.body.type,
      favorite: req.body.favorite

    }, {new: true})
    .then(crud => {

      if(!crud) {
            return res.status(404).send({
                message: "data not found with id " + req.params.crudId
            });
        }

        res.send(crud);
    }).catch(err => {
      res.status(500).send({
           message: err.message || "Some error occurred while updating the data."
       });
    });

};


exports.delete = (req, res) => {
    Crud.findByIdAndRemove(req.params.crudId)
    .then(crud => {
      if(!crud) {
            return res.status(404).send({
                message: "data not found with id " + req.params.crudId
            });
        }
         res.send({message: "Data removed successfully!"});
    }).catch(err => {
      return res.status(500).send({
              message: "Could not delete data with id " + req.params.crudId
          });
    });
};

var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var app = express();

mongoose.connect('mongodb://localhost:27017/zen3_test', {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


require('./routes/crud.routes.js')(app);

app.listen(3000, function() {
    console.log('Node.js listening on port ' + 3000)
});
